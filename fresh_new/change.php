<!DOCTYPE html>
<html>
<head>
	<title>Changes</title>
	<style type="text/css">
		ul {
	    list-style-type: none;
	    margin: 0;
	    padding: 0;
	    overflow: hidden;
	    background-color: #333;
		}

		li {
		    float: left;
		}

		li a {
		    display: block;
		    color: white;
		    text-align: center;
		    padding: 14px 16px;
		    text-decoration: none;
		}

		li a:hover:not(.active) {
		    background-color: #111;
		}

		.active {
		    background-color: #4CAF50;
		}
		body {
			width: 100wh;
			height: 90vh;
			color: #fff;
			background: linear-gradient(-40deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			-webkit-animation: Gradient 7s ease infinite;
			-moz-animation: Gradient 7s ease infinite;
			animation: Gradient 7s ease infinite;
		}

		@-webkit-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@-moz-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}
	</style>
</head>
<body>
	<ul>
	  <li><a href="/admin.php">Home</a></li>
	  <li><a href="/ad_book.php">Book</a></li>
	  <li><a href="/ad_query.php">Query</a></li>
	  <li><a class='active' href="/change.php">Make change</a></li>
	  <li style="float:right"><a href="">Log_in</a></li>
	  <li style="float:right"><a href="">Register</a></li>
	</ul>
	<form id="nameform" action="makedb.php" method="post">
		<fieldset>
			<legend>Changes:</legend>
			ADD/Delete:<br>
			<input type="radio" name="type" value="add" checked> Add
			<input type="radio" name="type" value="delete"> Delete<br>
			Train name:<br>
			<input type="text" name="name">
			<br>
			Train Id:<br>
			<input type="text" name="id">
			<br>
			Source:<br>
			<input type="text" name="source">
			<br>
			Destination:<br>
			<input type="text" name="destination">
			<br>
			Available Days:<br>
			<input type="text" name="days">
			<br>
			Fare_class_1:<br>
			<input type="number" name="rate1">
			<br>
			Fare_class_2:<br>
			<input type="number" name="rate2">
			<br>
			Fare_class_3:<br>
			<input type="number" name="rate3">
			<br>
			Seat_class_1_Number:<br>
			<input type="number" name="seat1">
			<br>
			Seat_class_2_Number:<br>
			<input type="number" name="seat2">
			<br>
			Seat_class_3_Number:<br>
			<input type="number" name="seat3">
			<br>
			Arrival Time:<br>
			<input type="time" name="at">
			<br>
			Departure Time:<br>
			<input type="time" name="dt">
			<br>
			<input type="submit" name="change" value="change">
		</fieldset>
	</form> 
</body>
</html>