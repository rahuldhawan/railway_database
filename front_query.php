<!DOCTYPE html>
<html>
<head>
	<title>RR-Ticket Reservation</title>
	<style type="text/css">
		ul {
	    list-style-type: none;
	    margin: 0;
	    padding: 0;
	    overflow: hidden;
	    background-color: #333;
		}

		li {
		    float: left;
		}

		li a {
		    display: block;
		    color: white;
		    text-align: center;
		    padding: 14px 16px;
		    text-decoration: none;
		}

		li a:hover:not(.active) {
		    background-color: #111;
		}

		.active {
		    background-color: #4CAF50;
		}
		body {
			width: 100wh;
			height: 90vh;
			color: #fff;
			background: linear-gradient(-40deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			-webkit-animation: Gradient 7s ease infinite;
			-moz-animation: Gradient 7s ease infinite;
			animation: Gradient 7s ease infinite;
		}

		@-webkit-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@-moz-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		h1,
		h6 {
			font-family: 'Open Sans';
			font-weight: 300;
			text-align: center;
			position: ;
			top: 45%;
			right: 0;
			left: 0;
		}


		.button {
		  border-radius: 4px;
		  background-color: #E73C7E;
		  border: none;
		  color: #FFFFFF;
		  text-align: center;
		  font-size: 28px;
		  padding: 20px;
		  width: 327px;
		  transition: all 0.5s;
		  cursor: pointer;
		  margin: 5px;
		}

		.button span {
		  cursor: pointer;
		  display: inline-block;
		  position: relative;
		  transition: 0.5s;
		}

		.button span:after {
		  content: '\00bb';
		  position: absolute;
		  opacity: 0;
		  top: 0;
		  right: -20px;
		  transition: 0.5s;
		}

		.button:hover span {
		  padding-right: 25px;
		}

		.button:hover span:after {
		  opacity: 1;
		  right: 0;
		}
	</style>
</head>
<body>
	<ul>
	  <li><a href="/home/breezy/front.html">Home</a></li>
	  <li><a href="/home/breezy/front_book.html">Book</a></li>
	  <li><a class='active' href="/home/breezy/front_query.html">Query</a></li>
	  <li><a href="">About</a></li>
	  <li style="float:right"><a href="">Log_in</a></li>
	  <li style="float:right"><a href="">Register</a></li>
	</ul> 
	<h1>select what you want to query?</h1>
	<ul>
		<li><button class="button" onclick="insert()"><span>Trains bw Stn</span></button></li>
		<li><button class="button" onclick="insert()"><span>Min Fare of Train</span></button></li>
		<li><button class="button" onclick="insert()"><span>Balance</span></button></li>
		<li><button class="button" onclick="insert()"><span>Stations Has Lift</span></button></li>
	</ul>
	<?php
		function select(){
   			echo "The select function is called.";
		}
		function insert(){
   			echo "The insert function is called.";
		}
	?>
</body>
</html>