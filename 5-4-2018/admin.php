<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>

<head>
  <title>RR-Ticket Reservation</title>
  <style type="text/css">
	h1 {
    	color: white;
    	text-shadow: 1px 1px 2px black, 0 0 25px blue, 0 0 5px darkblue;
	} 
	ul {
    list-style-type: none;
    margin: 0;
    padding: 0;
    overflow: hidden;
    background-color: #333;
	}

	li {
	    float: left;
	}

	li a {
	    display: block;
	    color: white;
	    text-align: center;
	    padding: 14px 16px;
	    text-decoration: none;
	}

	li a:hover:not(.active) {
	    background-color: #111;
	}

	.active {
	    background-color: #4CAF50;
	}

	
	body {
	margin: 0 !important;
	padding: 0 !important;
	overflow: hidden !important;
	}
	div {
		margin: 0 !important;
		padding: 0 !important;
	}
	.wrapper {
		height: 1080px;
		width: 1920px;
		margin: auto;
		background: #280744;
		overflow: hidden !important;
	}
	.background {
		height: 1080px;
		width: 1920px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QSXhkaG1VaEhSTjA
	);
		background-repeat: repeat-x;
		background-position: top center;
		background-size: contain;
	}

	.rocks_1 {
		position: absolute;
		bottom: -5px;
		height: 315px;
		width: 1920px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QckJsSTBOSmdsYmc);
		background-repeat: repeat-x;
		background-position: top center;
		background-size: contain;
		animation: translateX 100s infinite linear both;
	}
	.rocks_2 {
		position: absolute;
		bottom: -60px;
		height: 315px;
		width: 1920px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QSC1sYnVUWUxKSGs);
		background-repeat: repeat-x;
		background-position: top center;
		background-size: contain;
		animation: translateX 30s infinite linear both;
	}
	.rails {
		position: absolute;
		bottom: -110px;
		height: 315px;
		width: 1920px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QZmtCbjlIbjVwclk);
		background-repeat: repeat-x;
		background-position: top center;
		background-size: contain;
		animation: translateX 5s infinite linear both;
	}
	.train {
		position: absolute;
		bottom: -105px;
		height: 315px;
		width: 1500px;
		right: 0px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QandQOVZSTjBETGs);
		background-repeat: no-repeat;
		background-position: top center;
		background-size: contain;
	}
	.ground {
	  position: absolute;
	  width: 1920px;
	  height: 98px;
	  bottom: 0px;
	  background: #16012f;
	}
	.lights {
		position: absolute;
		bottom: -39px;
		height: 315px;
		width: 1920px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QNTJ1U2M1eFptSTA);
		background-repeat: repeat-x;
		background-position: top center;
		background-size: contain;
		animation: translateX 4s infinite linear both;
	}
	.moon {
		position: absolute;
		top: 20%;
		height: 315px;
		left: 200px;
		width: 120px;
		background: url(https://docs.google.com/uc?export=download&id=0B2PO2Lr7Bv2QRTVYeVFoYVppeUU);
		background-repeat: no-repeat;
		background-position: top center;
		background-size: contain;
	}

	@keyframes translateX {
		0% {background-position: 0px 0px;}
		100% {background-position:  1920px 0px;}
	}
	
  </style>
  <meta charset="UTF-8">
</head>

<body> 
	 <ul>
	  <li><a class='active' href="/admin.php">Home</a></li>
	  <li><a href="/ad_book.php">Book</a></li>
	  <li><a href="/ad_query.php">Query</a></li>
	  <li><a href="/change.php">Make change</a></li>
	  <li style="float:right"><a href=""><?php echo $_SESSION['login_user']; ?></a></li>
	</ul> 
	<div class="wrapper">
	  <div class="background"></div>
	  <div class="rocks_1"></div>
	  <div class="rocks_2"></div>
	  <div class="rails"></div>
	  <div class="train"></div>
	  <div class="ground"></div>
	  <div class="lights"></div>  
	  <div class="moon"></div>
	</div>

</body>