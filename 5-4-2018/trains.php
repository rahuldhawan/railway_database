<!DOCTYPE html>
<html>
<head>
	<title>Answer-Query</title>
</head>
<body>
	<?php
		$servername = "localhost";
		$username = "root";
		$password = "DON";
		$dbname = "railway";

		// Create connection
		$conn = mysqli_connect($servername, $username, $password, $dbname);
		// Check connection
		if (!$conn) {
			die("Connection failed: " . mysqli_connect_error());
		}
		$s = $_POST['Source'];
		$d = $_POST['Destination'];
		$sql = "call find_train($s,$d)";
		$result = mysqli_query($conn, $sql);

		if (mysqli_num_rows($result) > 0) {
			// output data of each row
			while($row = mysqli_fetch_assoc($result)) {
			    echo "id: " . $row["train_id"].  "<br>";
			}
		} else {
			echo "0 results";
		}

		mysqli_close($conn);
	?>
</body>
</html>