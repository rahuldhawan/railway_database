
<?php 
	session_start();
 ?>
<!DOCTYPE html>
<html>
<head>
	<title>RR_Ticket System</title>
	<style type="text/css">
		ul {
	    list-style-type: none;
	    margin: 0;
	    padding: 0;
	    overflow: hidden;
	    background-color: #333;
		}

		li {
		    float: left;
		}

		li a {
		    display: block;
		    color: white;
		    text-align: center;
		    padding: 14px 16px;
		    text-decoration: none;
		}

		li a:hover:not(.active) {
		    background-color: #111;
		}

		.active {
		    background-color: #4CAF50;
		}
		body {
			width: 100wh;
			height: 90vh;
			color: #fff;
			background: linear-gradient(-40deg, #EE7752, #E73C7E, #23A6D5, #23D5AB);
			background-size: 400% 400%;
			-webkit-animation: Gradient 7s ease infinite;
			-moz-animation: Gradient 7s ease infinite;
			animation: Gradient 7s ease infinite;
		}

		@-webkit-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@-moz-keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		@keyframes Gradient {
			0% {
				background-position: 0% 50%
			}
			50% {
				background-position: 100% 50%
			}
			100% {
				background-position: 0% 50%
			}
		}

		h1,
		h6 {
			font-family: 'Open Sans';
			font-weight: 300;
			text-align: center;
			position: ;
			top: 45%;
			right: 0;
			left: 0;
		}

		button {
		    background-color: #4dff88; /* Green */
		    border: none;
		    color: white;
		    padding: 15px 32px;
		    text-align: center;
		    text-decoration: none;
		    display: inline-block;
		    font-size: 16px;
		    margin: 4px 2px;
		    cursor: pointer;
		    -webkit-transition-duration: 0.4s; /* Safari */
		    transition-duration: 0.4s;
		    margin-left: 600px;
		}

		.button1 {
		    box-shadow: 0 8px 16px 0 rgba(0,0,0,0.2), 0 6px 20px 0 rgba(0,0,0,0.19);
		}
	</style>
</head>
<body>
	<ul>
	  <li><a href="test11.php">Home</a></li>
	  <li><a class='active' href="test3.php">Book</a></li>
	  <li><a href="test111.php">Query</a></li>
	  <li><a href="test2.php">About</a></li>
	  <li style="float:right"><a href=""><?php echo $_SESSION['login_user']; ?></a></li>
	</ul> 
	<br><br>
	<form id="nameform">
		<fieldset>
			<legend>Passenger-Information:</legend>
			First name:<br>
			<input type="text" name="firstname">
			<br>
			Last name:<br>
			<input type="text" name="lastname">
			<br>
			Address:<br>
			<input type="text" name="address">
			<br>
			Phone:<br>
			<input type="number" name="phone">
			<br>
			Gender:<br>
			<input type="radio" name="gender" value="male" checked> Male
			<input type="radio" name="gender" value="female"> Female
  			<input type="radio" name="gender" value="other"> Other<br>
		</fieldset>
		<br><br>
		<fieldset>
			<legend>Travelling-Information</legend>
			Source-Station:<br>
			<input type="text" name="source">
			<br>
			Destination_station:<br>
			<input type="text" name="destination">
			<br>
			Class:<br>
			<input type="text" name="value" value="1/2/3">
			<br>
			Date-of-Journey:<br>
			<input type="date" name="br">
			<br>
			Train no:<br>
			<input type="number" name="train_no">
			<br>
		</fieldset>
		<br>
	</form>
	<button type="submit" form="nameform" value="Submit" class="button1">Submit</button>
</body>
</html>